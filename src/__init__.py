from .editor import init_editor
from .main import init_main_window

def init():
    init_editor()
    init_main_window()
